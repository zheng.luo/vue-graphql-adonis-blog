const { makeExecutableSchema } = require('graphql-tools');

const typeDefs = `
  type User {
    id: Int!
    username: String!
    email: String!
    posts: [Post]
  }
  
  type Post {
    id: Int!
    title: String!
    slug: String!
    content: String!
    user: User!
  }

  type Query {
    allUsers: [User]
    userById(id: Int!): User
    allPosts: [Post]
    postById(id: Int!): Post
  }
  
  type Mutation {
    login(email: String!, password: String!): String
    createUser(username: String!, email: String!, password: String!): User
    addPost(title: String!, content: String!): Post
  }
`;

const resolvers = require('./resolvers');

module.exports = makeExecutableSchema({ typeDefs, resolvers });